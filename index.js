var express = require("express");
var bodyParser = require("body-parser");
var session = require("express-session");

var router_client = require("./routers/router_client");
var router_api_inventory = require("./routers/router_api_inventory");

var app = express();

//Archivos estaticos
app.use(express.static('public'));

// Configuracion para peticiones de tipo json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(session({
	secret: "secret_key",
	resave: false, // Si la session se modifica se vuelve a guardar tras la peticion
	saveUninitialized: false // cuando es nueva y no ha sido modificada
}));

// Configuracion para el manejo del renderizado en pug
app.set('view engine', 'pug');


//------------------Routing-----------------

app.use("/", router_client);
app.use("/api/inventory", router_api_inventory);



//Configuracion del puerto de la app
app.listen(8000);