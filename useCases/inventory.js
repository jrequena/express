
var GeneratorSchema = require("../models/generator").GeneratorSchema;



//caso de uso para Guardar un generador dado un conjunto de datos
var setGenerator = function(req, res){


	var generator = new GeneratorSchema(
		{
			maker_id : req.body.maker_id, 
			energy_consumption : req.body.energy_consumption, 
			breaking_force : req.body.breaking_force, 
			minimum_reaction_force : req.body.minimum_reaction_force, 
			shield_type : req.body.shield_type, 
			release_date : req.body.release_date, 
			estimated_lifetime : req.body.estimated_lifetime
		});

	// Creando una promesa 
	generator.save().then(
		function(us){ // Si todo sale bien
	    	res.send({"response": true});
		},
		function(err){ // Si algo salio mal
			res.status(400);
	    	res.send(JSON.stringify(err));
			
		}
	)
};




//caso de uso para devolver un listado de generadores
var getGenerators = function(req, res){
	// Manejo de una busqueda dentro de mongo 
	// ({},"", function)
	// {} -> Manejo de las restricciones de busqueda
	// "" -> Los campos que quieres q devuelva
	// function -> La funcion manejando los errors o los documentos resultantes.
	//User.find({},"username",function(err,doc){

	GeneratorSchema.find({},function(err,doc){
		res.send(JSON.stringify(doc));
	}).sort({_id:-1}).limit(10);
};




//caso de uso para buscar un generador dado su make_id
var getGeneratorsBy = function(req, res){
	var search_by = {
		maker_id: req.body.maker_id 
	};

	GeneratorSchema.find(search_by, function(err,doc){
		res.send(JSON.stringify(doc));
	});
};



module.exports.getGenerators = getGenerators;
module.exports.getGeneratorsBy = getGeneratorsBy;
module.exports.setGenerator = setGenerator;