var express = require("express");

var router = express.Router();
var inventoryUseCase = require("../useCases/inventory");
var GeneratorsSchema = require("../models/generator").GeneratorsSchema;



//Manejo de rutas tipo API para guardar un generador dentro de mongodb
router.post("/set/generators", function(req, res){
	res.setHeader('Content-Type', 'application/json');
	inventoryUseCase.setGenerator(req, res);

});


//Manejo de rutas tipo API para lista los generadores guardados dentro de mongodb
router.post("/get/generators", function(req, res){
	res.setHeader('Content-Type', 'application/json');
	inventoryUseCase.getGenerators(req, res);

});


//Manejo de rutas tipo API para buscar un generador dentro de mongodb dado un maker_id
router.post("/getby/generators", function(req, res){
	res.setHeader('Content-Type', 'application/json');
	inventoryUseCase.getGeneratorsBy(req, res);

});

module.exports = router;