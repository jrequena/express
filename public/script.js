
//----------------------------
//----------------------------

//-----------MAnejo de modal
// Get the modal
var modal = document.getElementById('myModal');

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

//----------------------------
//----------------------------




//Mandar el listado de generadores
$("#sendData").click(function(){
	$.ajax({
	    type: "POST",
	    dataType: "json",
	    url: "http://127.0.0.1:8000/api/inventory/get/generators",
	}).done(function( data, textStatus, jqXHR ) {

		var output = "";
        var c = 0;
        // recorremos cada usuario
        $.each(data, function(key, value) {

			if (c%2 == 0)
				css_class ="blue";
			else
				css_class ="red";

			if(value['shield_type'] == 0)
				shield_type = "Rayos";
			else
				shield_type = "Partículas";


            output += "<ol class='" + css_class + "'>\
				<li><b>maker_id: </b>" + value['maker_id'] + "</li>\
				<li><b>energy_consumption: </b>" + value['energy_consumption'] + " Watts </li>\
				<li><b>breaking_force: </b>" + value['breaking_force'] + " Newtons </li>\
				<li><b>minimum_reaction_force: </b>" + value['minimum_reaction_force'] + " Newtons </li>\
				<li><b>shield_type: </b>" +  shield_type  + "</li>\
				<li><b>release_date: </b>" +  new Date(value['release_date']).toLocaleString() + "</li>\
				<li><b>estimated_lifetime: </b>" + value['estimated_lifetime'] + " Años </li>\
			</ol>";
            c++;
        });
        
        // Actualizamos el HTML del elemento con id="#response-container"
        $("#response-container").html(output);
	 })
	 .fail(function( jqXHR, textStatus, errorThrown ) {
	     if ( console && console.log ) {
	         console.log( "La solicitud a fallado: " +  textStatus);
	     }
	});

});

//Guardar Un Generador
$("#setGenerator").click(function(){
	var json = {
		"maker_id":document.getElementById('maker_id').value,
		"energy_consumption":document.getElementById('energy_consumption').value,
		"breaking_force":document.getElementById('breaking_force').value,
		"minimum_reaction_force":document.getElementById('minimum_reaction_force').value,
		"shield_type":document.querySelector('[name="shield_type"]:checked').value,
		"release_date":document.getElementById('release_date').value,
		"estimated_lifetime":document.getElementById('estimated_lifetime').value

	};

	$.ajax({
	    data: json,
	    type: "POST",
	    dataType: "json",
	    url: "http://127.0.0.1:8000/api/inventory/set/generators",
	}).done(function( data, textStatus, jqXHR ) {
		document.querySelector('[class="modal-header"]').style.background = "green";
		document.querySelector('[class="modal-body"]').innerHTML = "Se ha guardado el generador";
		document.querySelector('[class="modal-footer"]').style.background = "green";
		modal.style.display = "block";
	})
		.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			document.querySelector('[class="modal-header"]').style.background = "red";
			document.querySelector('[class="modal-body"]').innerHTML = "No se a guardado el Formnulario. Chequee sus datos.";
			document.querySelector('[class="modal-footer"]').style.background = "red";
			modal.style.display = "block";
			console.log( "La solicitud a fallado: " +  textStatus);
		}
	});
});
