var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// Conexion a la base de datos
mongoose.connect("mongodb://localhost/invetoryDB");


// Configuracion del schema para el manejo del store de bd
var generator_schema = new Schema({
	// Id del fabricante: una cadena hexadecimal
	maker_id: {
		type: String, 
		required: "El maker_id es obligatorio",
		maxlength: [10, "El maker_id no puede ser mayor a 10 caracteres."]
	},
	// Consumo de energía en watts
	energy_consumption: {
		type: Number,
		required: "El energy_consumption es obligatorio",
		min: [
			1,
			"Valor minimo: 1"
		]
	},
	// Fuerza de ruptura en newtons
	breaking_force: {
		type: Number,
		required: "El breaking_force es obligatorio",
		min: [
			1,
			"Valor minimo: 1"
		]
	},
	// Fuerza mínima de reacción en newtons
	minimum_reaction_force: {
		type: Number,
		required: "El minimum_reaction_force es obligatorio",
		min: [
			1,
			"Valor minimo: 1"
		]
	},
	//Tipo de escudo: rayos o partículas
	shield_type: {
		type: String,
		required: "El shield_type es obligatorio",
		enum:{
			values: [0, 1],
			message: "Valores aceptados: Rayos o Particulas"
		}
	},
	// Fecha de lanzamiento
	release_date: {
		type: Date,
		required: "El release_date es obligatorio"
	},
	// Tiempo de vida estimado
	estimated_lifetime: {
		type: Number,
		required: "El estimated_lifetime es obligatorio",
		min: [
			1,
			"Valor minimo: 0"
		],
		max: [
			100,
			"Valor maximo: 10"
		]
	}

});

var GeneratorSchema = mongoose.model("Generator", generator_schema); 



module.exports.GeneratorSchema = GeneratorSchema;